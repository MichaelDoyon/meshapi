using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine;

[BurstCompile]
struct CoinUpdateJob : IJobParallelFor
{
    NativeArray<Coin> coins;
    float deltaTime;

    public CoinUpdateJob(NativeArray<Coin> c, float dt)
      => (coins, deltaTime) = (c, dt);

    public void Execute(int i)
        => coins[i] = coins[i].NextFrame(deltaTime);
    /*{
        coins[i] = coins[i].NextFrame(deltaTime);
    }*/
}

[BurstCompile]
struct CoinCleanUp : IJob
{
    NativeArray<Coin> coins;
    NativeArray<CoinGroupInfo> group;
    float bounds;

    public CoinCleanUp(NativeArray<Coin> coins, NativeArray<CoinGroupInfo> group, float bounds)
    {
        this.coins = coins;
        this.group = group;
        this.bounds = bounds;
    }

    public void Execute()
    {
        var bound1 = math.float2(-bounds, -1);
        var bound2 = math.float2(+bounds, +1);

        var actives = group[0].ActiveCount;
        var written = 0;

        for (var i = 0; i < actives; i++)
        {
            var p = coins[i].Position;

            if (math.any(p < bound1)) continue;
            if (math.any(p > bound2)) continue;

            if (i != written) coins[written] = coins[i];
            written++;
        }

        group[0] = CoinGroupInfo.ChangeActiveCount(group[0], written);
    }
}

[BurstCompile]
struct CoinSpawnJob : IJob
{
    NativeArray<Coin> coins;
    NativeArray<CoinGroupInfo> group;
    float2 pos;
    int count;

    public CoinSpawnJob(NativeArray<Coin> coins, NativeArray<CoinGroupInfo> group, float2 pos, int count)
    {
        this.coins = coins;
        this.group = group;
        this.pos = pos;
        this.count = count;
    }

    public void Execute()
    {
        var seed = group[0].SpawnCount + 1;

        var actives = group[0].ActiveCount;
        var spawns = math.min(coins.Length - actives, count);

        for (var i = 0; i < spawns; i++)
            coins[actives + i] = Coin.Spawn(pos, seed + i);

        group[0] = CoinGroupInfo.AddActiveAndSpawnCount(group[0], spawns);
    }
}
