using Unity.Mathematics;
using UnityEngine;

public struct RHash
{
    #region private fields
    public uint Seed { get; set; }

    const uint PRIME32_1 = 2654435761U;
    const uint PRIME32_2 = 2246822519U;
    const uint PRIME32_3 = 3266489917U;
    const uint PRIME32_4 = 668265263U;
    const uint PRIME32_5 = 374761393U;
    #endregion

    public RHash(uint seed) => (this.Seed) = (seed);

    static uint CalculateHash(uint data, uint seed)
    {
        uint h32 = seed + PRIME32_5;
        h32 += 4U;
        h32 += data * PRIME32_3;
        h32 = rotl32(h32, 17) * PRIME32_4;
        h32 ^= h32 >> 15;
        h32 *= PRIME32_2;
        h32 ^= h32 >> 13;
        h32 *= PRIME32_3;
        h32 ^= h32 >> 16;
        return h32;
    }
    static uint rotl32(uint x, int r)
    {
        return (x << r) | (x >> 32 - r);
    }

    #region Publics
    public uint UInt(uint data)
    {
        return CalculateHash(data, Seed);
    }
    public uint UInt(uint max, uint data)
    {
        return UInt(data) % max;
    }

    public float Float(uint data)
    {
        return UInt(data) / (float)uint.MaxValue;
    }
    public float Float(float max, uint data)
    {
        return Float(data) * max;
    }
    public float Float(float min, float max, uint data)
    {
        return Float(data) * (max - min) + min;
    }

    public float3 Float3(uint data)
    {
        return math.float3(
            Float(data),
            Float(data + 0x10000000),
            Float(data + 0x20000000)
        );
    }
    public float3 Float3(float3 max, uint data)
    {
        return Float3(data) * max;
    }
    public float3 Float3(float3 min, float3 max, uint data)
    {
        return Float3(data) * (max - min) + min;
    }
    #endregion
}
