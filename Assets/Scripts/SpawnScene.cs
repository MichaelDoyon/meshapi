using TMPro;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

public class SpawnScene : MonoBehaviour
{
    [SerializeField] Material diamondMat;
    [SerializeField] TextMeshProUGUI countText;
    [SerializeField] float diamondSize = 0.02f;

    private const int maxDiamondCount = 1000000;
    NativeArray<Coin> coins;
    NativeArray<CoinGroupInfo> coinGroup;
    Mesh sharedMesh;

    private void Start()
    { 
        Application.targetFrameRate = 60;

        coins = new NativeArray<Coin>(maxDiamondCount, Allocator.Persistent);
        coinGroup = new NativeArray<CoinGroupInfo>(1, Allocator.Persistent);
        coinGroup[0] = CoinGroupInfo.InitialState;

        sharedMesh = new Mesh();
        sharedMesh.indexFormat = IndexFormat.UInt32;
        sharedMesh.MarkDynamic();
    }

    private void OnDisable()
    {
        coins.Dispose();
        coinGroup.Dispose();
    }

    private void OnDestroy()
    { 
        Destroy(sharedMesh);
    }

    private void Update()
    {
        var dt = Time.deltaTime;
        var aspect = (float)Screen.width / Screen.height;
        var pos = math.float3(transform.position).xy;
        var spawn = Time.deltaTime < 1.0f / 58 ? 400 : 20;

        // Bullet update job chain
        var handle = new CoinUpdateJob(coins, dt).Schedule(coinGroup[0].ActiveCount, 64);
        handle = new CoinCleanUp(coins, coinGroup, aspect).Schedule(handle);
        handle = new CoinSpawnJob(coins, coinGroup, pos, spawn).Schedule(handle);
        handle.Complete();

        NativeSlice<Coin> ActiveBatch = new NativeSlice<Coin>(coins,0,coinGroup[0].ActiveCount);
        //MeshBuilderSimple.Build(ActiveBatch, diamondSize, sharedMesh);
        MeshBuilderAdvanced.Build(ActiveBatch, diamondSize, sharedMesh);

        // Draw call
        Graphics.DrawMesh(sharedMesh, Vector3.zero, Quaternion.identity, diamondMat, 0);

        // UI update
        countText.text = $"{coinGroup[0].ActiveCount} coins";
    }
}
